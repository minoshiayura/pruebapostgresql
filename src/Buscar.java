import java.sql.*;
import java.util.Scanner;

public class Buscar {

    public void buscar(){

        try{
            Conexion con = Conexion.getInstance();

            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce la matricula del vehiculo a buscar.\n" +
                    "Recuerda que son 4 números y 3 letras: ");
            String license_num = scanner.nextLine();

            try{
                String sql = "SELECT * FROM vehicle WHERE license_num = ?";
                PreparedStatement stmt = con.getConexion().prepareStatement(sql);
                stmt.setString(1,license_num);

                ResultSet rs = stmt.executeQuery();
                if(rs.next()){
                    int id = rs.getInt(1);
                    String license = rs.getString(2);
                    int price_gross = rs.getInt(4);
                    System.out.println("id: "+id+", matricula: "+license+", precio: "+price_gross);
                }else{
                    System.out.println("Vehiculo especificado no encontrado.");
                }
                rs.close();
                stmt.close();
            } catch (SQLException sqle){
                System.out.println("Error al buscar registro: "+sqle.getMessage());
            }
        }catch(Exception sqle){
            System.out.println("Error al conectar con la base de datos: "+sqle.getMessage());
        }finally {
            Menu menu = new Menu();
            menu.menu();

        }
    }
}
