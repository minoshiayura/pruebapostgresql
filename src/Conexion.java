import java.sql.*;

public class Conexion{

    private static Conexion instance =null;
    private String url = "jdbc:postgresql://localhost:5432/concesionario";
    private String user = "postgres";
    private String password = "postgresql";
    private Connection conexion = null;

    private Conexion(){
        try{
            Class.forName("org.postgresql.Driver");
            conexion = DriverManager.getConnection(url,user,password);
        }catch(ClassNotFoundException | SQLException sqle){
            System.out.println("Error al conectar con la BD: "+sqle.getMessage());
        }
    }

    public static Conexion getInstance(){
        if (instance == null){
            instance = new Conexion();
        }
        return instance;
    }

    public Connection getConexion(){
        return conexion;
    }

}
