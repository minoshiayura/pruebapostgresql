import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Eliminar {
    public void eliminar(){

        try{
            Conexion con = Conexion.getInstance();

            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce la matricula del vehiculo a eliminar.\n" +
                    "Recuerda que son 4 números y 3 letras: ");
            String license_num = scanner.nextLine();

            try{
                String sql = "DELETE FROM vehicle where license_num = ?";
                PreparedStatement statement = con.getConexion().prepareStatement(sql);
                statement.setString(1,license_num);

                int filasEliminadas = statement.executeUpdate();
                if(filasEliminadas > 0){
                    System.out.println("Eliminado registro con matricula: "+license_num);
                }
            } catch (SQLException sqle){
                System.out.println("Error al insertar nuevo registro: "+sqle.getMessage());
            }
        }catch(Exception sqle){
            System.out.println("Error al conectar con la base de datos: "+sqle.getMessage());
        }finally {
            Menu menu = new Menu();
            menu.menu();
        }
    }
}
