import java.sql.*;
import java.util.Scanner;

public class Anadir {

    public void anadir(){
        try{
            Conexion con = Conexion.getInstance();

            Scanner scanner = new Scanner(System.in);
            System.out.println("Introduce la matricula. Recuerda que son 4 números y 3 letras: ");
            String license_num = scanner.nextLine();
            System.out.println("Introduce el precio: ");
            int price_gross = scanner.nextInt();
            System.out.println("Introduce el fabricante: ");
            int manufacter_id = scanner.nextInt();
            System.out.println("Introduce el modelo: ");
            int model_id = scanner.nextInt();
            System.out.println("Introduce la version: ");
            int version_id = scanner.nextInt();
            System.out.println("Introduce el extra: ");
            int extra_id = scanner.nextInt();

            try{
                String sql = "INSERT INTO vehicle (license_num, price_gross, manufacturer_id, model_id, version_id, extra_id)\n" +
                        "VALUES (?, ?, ?, ?, ?, ?);";
                PreparedStatement statement = con.getConexion().prepareStatement(sql);
                statement.setString(1,license_num);
                statement.setInt(2,price_gross);
                statement.setInt(3,manufacter_id);
                statement.setInt(4,model_id);
                statement.setInt(5,version_id);
                statement.setInt(6,extra_id);
                int filasInsertadas = statement.executeUpdate();
                if(filasInsertadas > 0){
                    System.out.println("Añadido nuevo registro.");
                }
            } catch (SQLException sqle){
                System.out.println("Error al insertar nuevo registro: "+sqle.getMessage());
            }
        }catch(Exception sqle){
            System.out.println("Error al conectar con la base de datos: "+sqle.getMessage());
        }finally {
            Menu menu = new Menu();
            menu.menu();
                    }
    }
}
