import java.io.IOException;
import java.util.Scanner;

public class Menu {

    public void menu() {
        System.out.println("Menu de concesionario");
        System.out.println("1. Añadir vehiculos.");
        System.out.println("2. Eliminar vehiculos.");
        System.out.println("3. Mostrar vehiculos.");
        System.out.println("4. Buscar vehiculos.");
        System.out.println("5. Cerrar.");
        try{
            Scanner scanner = new Scanner(System.in);
            int c = scanner.nextInt();

            switch (c) {
                case 1:
                    System.out.println("Añadir vehiculo.");
                    Anadir anadir = new Anadir();
                    anadir.anadir();
                    break;
                case 2:
                    System.out.println("Eliminar vehiculo.");
                    Eliminar eliminar = new Eliminar();
                    eliminar.eliminar();
                    break;
                case 3:
                    System.out.println("Mostrar vehiculos.");
                    Mostrar mostrar = new Mostrar();
                    mostrar.mostrar();
                    break;
                case 4:
                    System.out.println("Buscar vehiculo.");
                    Buscar buscar = new Buscar();
                    buscar.buscar();
                    break;
                case 5:
                    System.out.println("Cerrando...");
                    System.exit(1);
                    break;
                default:
                    System.out.println("Seleccion erronea.");
                    this.menu();
            }
        }catch (Exception e){
            System.err.println("Error: "+e.getMessage());
        }

    }
}
