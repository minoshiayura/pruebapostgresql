import java.sql.*;
public class Mostrar {

    public void mostrar(){

        try{
            Conexion con = Conexion.getInstance();
            String sql= "SELECT * FROM vehicle";

            Statement stmt = con.getConexion().createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()){
                int id = rs.getInt(1);
                String license_num = rs.getString(2);
                int price_gross = rs.getInt(4);
                System.out.println("id: "+id+", matricula: "+license_num+", precio: "+price_gross);
            }

        }catch(SQLException sqle){
            System.out.println("Error: "+sqle.getMessage());
        }finally {
            Menu menu = new Menu();
            menu.menu();
        }
    }
}
